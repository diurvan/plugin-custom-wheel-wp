# plugin-custom-wheel-wp

Plugin para mostrar Ruleta en WordPress. Genera un Shortcode para colocarlo en cualquier sitio de la web una ruleta. Repositorio privado https://gitlab.com/diurvan/diu-custom-wheel-wp


![Imagen de Ruleta](https://gitlab.com/diurvan/plugin-custom-wheel-wp/-/raw/9a77fa7559b85dfdd1a7b85882108ef55fef835a/Wheel.png)

Este plugin carga a todos los usuarios de WordPress con un determinado rol, y los coloca en la ruleta, de manera aleatoria, y con un color aleatorio.
Luego, podemos girar la ruleta y saldrá un ganador!!
Plugin muy útil si queremos usarlo para sorteos o regalo.

Contribuye con tus comentarios.

Visita mi web en https://diurvanconsultores.com
